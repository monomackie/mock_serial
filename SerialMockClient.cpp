#include "SerialMockClient.h"
#include "mserialProtocol.h"
#include "Log.h"
#include <chrono>
#include <thread>

SerialMockClient::SerialMockClient(const SerialPort &serialPort):
        m_SerialPort(serialPort),
        m_State(SMC_STATE_DISCONNECTED),
        m_SoundController(SPEAKER_MODE),
        m_SelectedSession(-1), m_MistakesCount(0) {}

SerialMockClient::~SerialMockClient() {
    m_SoundController.closeDevice();
}

void SerialMockClient::pingToClient() {
    unsigned char buffer = HW_PING_TO_HOST_FUNCCODE;
    m_SerialPort.writeByte(&buffer);

    if (m_SerialPort.readByte(&buffer) > 0 && buffer == SW_PONG_TO_DEVICE_FUNCCODE) {
        m_State = SMC_STATE_CONNECTED;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
}

void SerialMockClient::sendDataToClient(SerialData sd) {
    if (sd.type == HW_POTENTIOMETER_FUNCCODE) {
        unsigned char high = sd.data >> 8;
        unsigned char low = sd.data & 0xff;
        unsigned char pot_val[3] = {HW_POTENTIOMETER_FUNCCODE, high, low};
        m_SerialPort.writeBytes(pot_val, 3);
    }
}

void SerialMockClient::leftButtonClick() {
    unsigned char data[2] = {HW_SWITCH_FOCUS_FUNCCODE, HW_SWITCH_FOCUS_BACKWARD};
    m_SerialPort.writeBytes(data, 2);

    m_SoundController.updateSessionEnumerator();
    if (m_SelectedSession <= -1)
        m_SelectedSession = m_SoundController.getSessionCount();
    else
        m_SelectedSession--;
}

void SerialMockClient::rightButtonClick() {
    unsigned char data[2] = {HW_SWITCH_FOCUS_FUNCCODE, HW_SWITCH_FOCUS_FORWARD};
    m_SerialPort.writeBytes(data, 2);

    m_SoundController.updateSessionEnumerator();
    if (m_SelectedSession >= m_SoundController.getSessionCount())
        m_SelectedSession = -1;
    else
        m_SelectedSession++;
}

void SerialMockClient::movePotentiometerTo(int value) {
    unsigned char high = value >> 8;
    unsigned char low = value & 0xff;
    unsigned char data[3] = {HW_POTENTIOMETER_FUNCCODE, high, low};
    m_SerialPort.writeBytes(data, 3);
}

void SerialMockClient::waitMillis(int millis) {
    std::this_thread::sleep_for(std::chrono::milliseconds(millis));
}

unsigned int SerialMockClient::getState() const {
    return m_State;
}

int SerialMockClient::getSelectedSession() const {
    return m_SelectedSession;
}

void SerialMockClient::checkTargetVolume(int target, int waitTime) {
    float actual;
    if (m_SelectedSession < 0)
        actual = m_SoundController.getMasterVolume();
    else
        actual = m_SoundController.getSessionVolume(m_SelectedSession);

    if (((float)target/1023.f) - actual >= 0.000001f) {
        Log::info("Set volume: %f | Actual volume: %f | Delay: %d\n", ((float)target/1023.f), actual, waitTime);
        m_MistakesCount++;
    }
}

int SerialMockClient::getMistakesCount() const {
    return m_MistakesCount;
}
